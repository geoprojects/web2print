<?php 
/**
 * Template displaying maps
 */
$map_id = (int) get_the_ID();
?>

<section class="maps" data-order="<?php echo( get_post_meta( $map_id, 'paged-order', true ) ); ?>" data-title="<?php echo esc_attr( the_title() ); ?>">
    <header>
        <h2><?php the_title(); ?></h2>
    </header>

    <div class="content">
        <?php the_content(); ?>
    </div>

    <?php
        $map_settings = get_post_meta( $map_id, 'geoprint_map_settings', true );
        $zoom   = ! empty ( $map_settings ) && ! empty( $map_settings['zoom'] ) ? (int) $map_settings['zoom'] :  get_post_meta( $map_id, 'gp_tiles_zoom', true );
        $center = ! empty ( $map_settings ) && ! empty( $map_settings['center'] ) ? json_encode( $map_settings['center'] ) :  get_post_meta( $map_id, 'gp_tiles_center', true );
        printf(
            '<div class="map-container" id="%s" data-map="%s" data-zoom="%s" data-center="%s" data-tiles="%s"></div>',
            esc_attr( sprintf( 'map-%d', $map_id ) ),
            esc_attr( $map_id ),
            esc_attr( $zoom ),
            esc_attr( $center ),
            esc_attr( get_post_meta( $map_id, 'gp_tiles_provider', true ) ),
        );
        include geoprint_get_template( 'content/markers' );
    ?>
</section>
