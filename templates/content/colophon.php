<?php
/**
 * Template for displaying last page of the book.
 */
$project_id = get_the_ID();
$content    = get_post_meta( $project_id, 'geoprint_colophon_content', true );
if ( empty( $content ) ) return;
?>
<section class="colophon">
	<div class="wrapper content">
        <?php echo wp_kses_post( $content ); ?>
    </div>
</section>