<?php 
/**
 * Template displaying posts
 */
?>
<section data-order="<?php echo (int) get_post_meta( get_the_ID(), 'paged-order', true ); ?>" class="post" data-title="<?php echo esc_attr( the_title() ); ?>">
    <article>
        <header>
            <?php $meta_date = get_post_meta( get_the_ID(), 'meta-date', true ); ?>
            <?php $post_date = get_the_date( 'l j F Y' );  ?>
            <p class="date-entry"><?php echo( $post_date ) ?></p>
            <h2><?php the_title() ?></h2>
        </header>
        <?php the_content() ?>
    </article>
</section>