<?php
/**
 * Template displaying markers
 */
$map_id = (int) get_the_ID();

$marker_args = array(
    'post_type'       => array( 'markers' ),
    'meta_key'        => 'gp_map',
    'meta_value'      => $map_id,
    'meta_value_type' => 'numeric',
    'posts_per_page'  => -1
);

$marker_query = new WP_Query( $marker_args );

if ( $marker_query->have_posts() ) :  while ( $marker_query->have_posts() ) : $marker_query->the_post();

        $marker_id = (int) get_the_ID();
        $gp_lat    = get_post_meta( $marker_id, 'gp_lat', true );
        $gp_lng    = get_post_meta( $marker_id, 'gp_lng', true );
        $gp_map    = get_post_meta( $marker_id, 'gp_map', true );
        $gp_zoom   = get_post_meta( $marker_id, 'gp_tiles_zoom', true );
        $gp_tiles  = get_post_meta( $marker_id, 'gp_tiles_provider', true );
        $gp_icon_type      = get_post_meta( $marker_id, 'gp_icon_type', true );
        $gp_icon_filename  = get_post_meta( $marker_id, 'gp_icon_filename', true );
        $display_image_une = get_post_meta( $marker_id, 'display_image_une', true );
        
        $map_settings = get_post_meta( $marker_id, 'geoprint_map_settings', true );
        $zoom   = ! empty ( $map_settings ) && ! empty( $map_settings['zoom'] ) ? (int) $map_settings['zoom'] :  $gp_zoom;
        $center = ! empty ( $map_settings ) && ! empty( $map_settings['center'] ) ? json_encode( $map_settings['center'] ) :  get_post_meta( $marker_id, 'gp_tiles_center', true );

        // popup stuff
        $gp_popup_content_order = explode( ',', get_post_meta($marker_id, 'gp_popup_content_order', true ) );
        $gp_popup_text = get_post_meta( $marker_id, 'gp_popup_text', true );
        $gp_popup_image = get_post_meta( $marker_id, 'gp_popup_image', true );
        $gp_popup_video = get_post_meta( $marker_id, 'gp_popup_video', true );
        $gp_popup_audio = get_post_meta( $marker_id, 'gp_popup_audio', true );

        // find the url for the marker icons
        $marker_icon_base_url = ( $gp_icon_type == 'default' ) ? GP_URL_DEFAULT_MARKERS_ICONS : GP_URL_CUSTOM_MARKERS_ICONS;
        $marker_icon_url = $marker_icon_base_url . '/' . $gp_icon_filename;
?>
        <section class="marker" data-marker-id="<?php echo $marker_id ?>" data-map="<?php echo esc_attr( $gp_map ) ?>" data-lat="<?php echo esc_attr( $gp_lat ); ?>" data-lng="<?php echo esc_attr( $gp_lng ); ?>" data-tiles-provider="<?php echo esc_attr( $gp_tiles ) ?>" data-popup-image="<?php echo esc_attr( $gp_popup_image ) ?>" data-popup-text="<?php echo esc_attr( $gp_popup_text ) ?>" data-popup-video="<?php echo esc_attr( $gp_popup_video ) ?>" data-popup-audio="<?php echo esc_attr( $gp_popup_audio ) ?>" data-icon="<?php echo esc_attr( $marker_icon_url ) ?>">
            <div class="map-marker">
                <?php
                    printf(
                        '<div class="map-container markeronly" id="map-marker-%s" data-marker-id="%s" data-map="%s" data-zoom="%s" data-center="%s" data-lat="%s" data-lng="%s" data-tiles="%s"></div>',
                        esc_attr( $gp_map . '-' . $marker_id ),
                        esc_attr( $marker_id ),
                        esc_attr( $gp_map ),
                        esc_attr( $zoom ),
                        esc_attr( $center ),
                        esc_attr( $gp_lat ),
                        esc_attr( $gp_lng ),
                        esc_attr( $gp_tiles ),
                    ); 
                ?>

                <!-- fetch latitude and longitude of the marker -->
                <p class="marker-data">
                    <span class="lat"><strong class="key"><?php _e( 'Latitude : ', 'geoprint' ); ?></strong><span class="value"><?php echo esc_html( $gp_lat ); ?></span></span>
                    <span class="lgn"><strong class="key"><?php _e( 'Longitude : ', 'geoprint' ); ?></strong><span class="value"><?php echo esc_html($gp_lng); ?></span></span>
                </p>
            </div>
            <h2><?php the_title(); ?></h2>
            <div class="content"><?php the_content(); ?></div>
        </section>

<?php endwhile; endif; wp_reset_postdata(); ?>