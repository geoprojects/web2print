<?php
/**
 * Template for displaying source HTML
 */
$args = geoprint_get_project_content_args( get_the_id(), [], true );
?>

<template id="pagedjs-source">
    <?php 
        // Include project stuff first
        $generate_cover = get_post_meta( (int) get_the_id(), 'geoprint_generate_cover', true );
        if( $generate_cover ) include geoprint_get_template( 'content/site-info' );
        include geoprint_get_template( 'content/project' );     
        
        // Include all content
        $query = new WP_Query( $args );
        if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post();
            $order = get_post_meta( get_the_id(), 'paged-order', true );
            if( $order ) include geoprint_get_template( 'content/' . get_post_type() );     
        endwhile; endif;
        wp_reset_postdata();

        // Add colophon
        $generate_colophon = get_post_meta( (int) get_the_id(), 'geoprint_generate_colophon', true );
        if( $generate_colophon ) include geoprint_get_template( 'content/colophon' );     
    ?>
</template>
