<?php
/** 
 * Plugin Name: Web To Print - Geoproject 
 * Description: Print your geoproject using PagedJS
 * Author:      Julien Taquet, Agathe Baez & Vincent Dubroeucq
 * Version:     1.0.0
 * Require PHP: 7.2
 * Requires at least: 5.6
 * License:     GPL v3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: geoprint
 * Domain Path: /languages
 **/

define( 'GEOPRINT_PATH', plugin_dir_path( __FILE__ ) );
define( 'GEOPRINT_URL', plugin_dir_url( __FILE__ ) );
define( 'GEOPRINT_VERSION', '1.0.0' );


add_action( 'init', 'geoprint_load_textdomain' );
/**
 * Load translations
 */
function geoprint_load_textdomain() {
    load_plugin_textdomain( 'geoprint', FALSE, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

include 'inc/functions.php';
include 'inc/post-types.php';
include 'inc/metaboxes.php';

add_filter( 'template_include', 'geoprint_template_include' );
/**
 * Filters the template to use the PagedJS preview template
 * 
 * @param   string  $template
 * @return  string  $template
 */
function geoprint_template_include( $template ){
    if( isset( $_GET['paged'] ) && 'yes' == $_GET['paged'] && 'projects' === get_post_type() ) {
        include 'inc/template-functions.php';
        include 'inc/template-tags.php';
        remove_all_filters( 'pre_get_posts' );
        remove_all_filters( 'posts_orderby', 99 );
        $template = geoprint_get_template( 'preview' );
    }
    return $template;
}

