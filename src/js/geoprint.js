import { Previewer, Handler } from 'pagedjs';
import { copyToClip, move, makeMaps, iframeToLinks, metaSlideToGrid, cleanImg, addIDtoEachElement } from './utils.js';

document.addEventListener('DOMContentLoaded', e => {

    const source = document.querySelector('#pagedjs-source');
    const target = document.body;
    const settings = document.querySelector('.pagedjs-settings');
    let previewContainer = null;

    class geoProject extends Handler {
        constructor(chunker, polisher, caller) {
            super(chunker, polisher, caller);
        }
        beforeParsed(content) {
            iframeToLinks(content);
            metaSlideToGrid(content);
            cleanImg(content);
            if(content.querySelector(".project-content p")) {
                content.querySelector(".project-content p").innerHTML = `<span>${content.querySelector(".project-content p").innerHTML}</span>`
            }   
            if(content.querySelector(".project-content pre")) {
                content.querySelector(".project-content pre").innerHTML = `<span>${content.querySelector(".project-content pre").innerHTML}</span>`
            }
            addIDtoEachElement(content);
        }
        afterRendered(pages) {
            makeMaps(target);
            move();
            document.querySelector('#copyMapData').addEventListener('click', function() {
                let data = this.parentElement.querySelector('.mapDataCss').textContent;
                copyToClip(data)
            })
            document.querySelectorAll('.close').forEach(close => {
                close.addEventListener('click', function() {
                    this.closest('.show').classList.remove("show");
                })
            })
        }
    }

    const previewer = new Previewer();
    previewer.registerHandlers(geoProject);
    const styles = formatStyles ? [formatStyles, baseStyles] : [baseStyles];
    let flow = previewer.preview(source.content, styles, target).then( flow => {
        console.log('Rendered', flow.total, 'pages.');
        previewContainer = document.querySelector(':root');
        registerHandlers();
    });
    
    const registerHandlers = () => {
        if(settings){
            const cssVarInputs = settings.querySelectorAll('.css-var-input');
            const cssClassInputs = settings.querySelectorAll('.css-class-input');
            const cssHelperInputs = settings.querySelectorAll('.helper-input');
            if (cssVarInputs){
                cssVarInputs.forEach(input => {
                    input.addEventListener( 'change', e => {
                        const value = e.target.value;
                        const cssVar = input.dataset.var;
                        const unit = input.dataset.unit || '';
                        const section = input.closest('.settings-section');
                        if ( ! section ) return;
                        const selector = section.dataset.selector;
                        const elements = document.querySelectorAll( selector );
                        if ( elements ) elements.forEach( element => element.style.setProperty( cssVar, value + unit) );
                    } );
                });
            }
            if (cssClassInputs){
                cssClassInputs.forEach(input => {
                    let optionValues = [...input.options].map(o => o.value);
                    input.addEventListener( 'change', e => {
                        const section = input.closest('.settings-section');
                        if ( ! section ) return;
                        const selector = section.dataset.selector;
                        const elements = document.querySelectorAll( selector );
                        elements.forEach( element => {
                            optionValues.forEach( val => { if (val) element.classList.remove( val ) } ),                          
                            element.classList.add( e.target.value );
                        });
                    } );
                });
            }
            if (cssHelperInputs){
                cssHelperInputs.forEach(helper => {
                    const targetId = helper.dataset.target;
                    const target   = document.querySelector( `#${targetId}` );
                    if( ! target ) return;
                    helper.addEventListener( 'change', e => {
                        target.value = e.target.value;
                        target.dispatchEvent(new Event('change'));
                    } );
                    target.addEventListener( 'change', e => helper.value = e.target.value );
                });
            }
        }
        
        // Settings overlay toggles
        const toggleButtons = document.querySelectorAll('.form-toggle');
        if(toggleButtons){
            toggleButtons.forEach(button => {
                button.addEventListener('click', e => {
                    const controls = button.getAttribute('aria-controls');
                    const overlay  = document.querySelector(`#${controls}`);
                    overlay.classList.toggle('open');
                    document.querySelectorAll(`[aria-controls="${controls}"]`).forEach(toggle => {
                        toggle.setAttribute('aria-expanded', overlay.classList.contains('open'));
                    });
                });
            });
        }
    
        // Sections toggles
        const sectionToggles = settings.querySelectorAll('.section-toggle');
        const sections = settings.querySelectorAll('.settings');
        if(sectionToggles){
            sectionToggles.forEach(button => {
                button.addEventListener('click', e => {
                    const controls = button.getAttribute('aria-controls');
                    const section  = document.querySelector(`#${controls}`);
                    
                    // Close all other sections
                    sections.forEach( panel => panel.id !== controls && panel.classList.remove('open'));
                    sectionToggles.forEach( toggle => toggle !== button && toggle.setAttribute('aria-expanded', false));
                    
                    // Open section
                    section.classList.toggle('open');
                    button.setAttribute('aria-expanded', section.classList.contains('open'));
                });
            });
        }
    
        // Print button
        const printButton = settings.querySelector('#print');
        if(printButton) printButton.addEventListener('click', e => window.print() );
    }
});
