<?php 
/**
 * Returns the path to a template file.
 * Looks first if the file exists in the `geoprint/` folder in the child theme,
 * then in the parent's theme `geoprint/` folder,
 * finally in the default plugin's template directory
 *
 * @param   string  $slug     The template we're looking for
 * @return  string  $located  The path to the template file if found.
 */
function geoprint_get_template( $slug ) {
    $located = '';
    
    $template_name = "{$slug}.php";
    if ( file_exists( STYLESHEETPATH . '/geoprint/' . $template_name ) ) {
        $located = STYLESHEETPATH . '/geoprint/' . $template_name;
    } elseif ( file_exists( TEMPLATEPATH . '/geoprint/' . $template_name ) ) {
        $located = TEMPLATEPATH . '/geoprint/' . $template_name;
    } elseif ( file_exists( GEOPRINT_PATH . 'templates/' . $template_name ) ) {
        $located = GEOPRINT_PATH . 'templates/' . $template_name;
    }

    return str_replace( '..', '', $located ) ;
}

/**
 * Returns the url to a CSS format file.
 * Looks first if the file exists in the `geoprint/formats` folder in the child theme,
 * then in the parent's theme `geoprint/formats` folder,
 * finally in the default plugin's formats directory
 *
 * @param   string  $format   The format we're looking for
 * @return  string  $located  The URL to the format file if found.
 */
function geoprint_get_format_url( $format ) {
    $located = '';
    
    if ( file_exists( get_theme_file_path( "/geoprint/formats/${format}" ) ) ) {
        $located = get_theme_file_uri( "/geoprint/formats/${format}" );
    } elseif ( file_exists( trailingslashit( GEOPRINT_PATH ) . "formats/${format}" ) ) {
        $located = trailingslashit( GEOPRINT_URL ) . "formats/${format}";
    }

    return str_replace( '..', '', $located ) ;
}


/**
 * Retrieve all content associated with a project.
 * 
 * @param   int    $post_id  Project ID.
 * @return  array  $content
 */
function geoprint_get_project_content( $post_id = 0, $additional_args = [] ){
    if( ! $post_id ) $post_id = get_the_ID();

    $content = [];

    if( 'projects' === get_post_type( $post_id ) ){
        $args    = geoprint_get_project_content_args( $post_id, $additional_args );
        $content = get_posts( $args );
    }

    return $content;
}


/**
 * Retrieve arguments to fetch content associated with a project.
 * 
 * @param   int    $post_id          Project ID.
 * @param   array  $additional_args  Additional args to merge or override.
 * @return  array  $args
 */
function geoprint_get_project_content_args( $post_id = 0, $additional_args = [], $sorted = false ){
    if( ! $post_id ) $post_id = get_the_ID();

    $args = [];
    if( 'projects' === get_post_type( $post_id ) ){
        $args = array_merge( array(
            'post_type'        => array( 'post', 'geoformat', 'maps', 'capes', 'markers' ),
            'posts_per_page'   => -1,
            'suppress_filters' => true,
            'meta_query'       => array(
                'project_clause' => array(
                    'key'   => 'gp_project',
                    'type'  => 'NUMERIC',
                    'value' => (int) $post_id,
                ),
            ),
        ), $additional_args );
    
        if( $sorted ) {
            $args['meta_key'] = 'paged-order';
            $args['orderby']  = 'meta_value_num';
            $args['order']    = 'ASC'; 
        }
    }
    return $args;
}


/**
 * Prints the content order inputs
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_content_order_inputs( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $html     = '';
    $inputs   = '';

    $contents = geoprint_get_project_content( $post_id );
    if( ! empty( $contents ) && is_array( $contents ) ){
        $max    = count( $contents );
        foreach ( $contents as $content ) {
            $value   = get_post_meta( (int) $content->ID, 'paged-order', true );
            /* translators: %1$s = URL, %2$s = title ; %3$s = post type */
            $label   = sprintf( __( 'Print order for <a href="%1$s" target="_blank">%2$s</a> <small>(%3$s)</small>', 'geoprint' ), esc_url( get_permalink( (int) $content->ID ) ), wp_kses_post( $content->post_title ), esc_html( $content->post_type ) );
            $inputs .= sprintf( 
                '<li style="display:flex;align-items: center;">
                    <label style="order:1; margin-left: 8px;">%s</label>
                    <input type="number" name="paged-order-%d" value="%d" min="0" max="%d" />
                </li>',
                $label,
                (int) $content->ID,
                esc_attr( (int) $value ),
                (int) $max
            );
        }
    }
    if( $inputs ) $html = sprintf( '<div class="settings-field content-order-field"><ul>%s</ul></div>', $inputs );

    $html = apply_filters( 'geoprint_content_order_inputs', $html, $post_id, $contents );
    if( $echo ) echo $html;
    return $html;
}


/**
 * Prints the CSS Template input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_css_template_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $templates = get_posts( array( 'post_type' => 'pagedjs-template', 'posts_per_page' => -1 ) );    
    $value     = (int) get_post_meta( (int) $post_id, 'chosenTemplate', true );
    $args      = array(
        'type'        => 'select',
        'name'        => 'chosenTemplate',
        'label'       => __( 'Choose a print template stylesheet for your story', 'geoprint' ),
        'class'       => 'hidden-content-field',
        'default'     => false,
        'description' => '',
        'value'       => $value,
        'options'     => wp_list_pluck( $templates, 'post_title', 'ID' )
    );
    geoprint_render_setting_field( 'chosenTemplate', $args );
}


/**
 * Prints the book format input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_format_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $formats = geoprint_get_book_formats();
    $value   = get_post_meta( (int) $post_id, 'geoprint_book_format', true );
    $args    = array(
        'type'        => 'select',
        'name'        => 'geoprint_book_format',
        'label'       => __( 'Choose the book format for your story', 'geoprint' ),
        'class'       => 'css-template-field',
        'default'     => '',
        'description' => '',
        'value'       => $value,
        'options'     => $formats
    );
    geoprint_render_setting_field( 'geoprint_book_format', $args );
}


/**
 * Prints the hide content input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_hidden_content_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $value = get_post_meta( (int) $post_id, 'geoprint_hidden_content', true );
    if( is_array( $value ) ) $value = join( ',', $value );
    $args  = array(
        'type'        => 'textarea',
        'name'        => 'geoprint_hidden_content',
        'label'       => __( 'Hidden content', 'geoprint' ),
        'class'       => 'css-hidden-content-field',
        'default'     => '',
        'description' => __( 'Enter the id of the elements you want to hide, seperated by commas.', 'geoprint' ),
        'value'       => $value,
    );
    geoprint_render_setting_field( 'geoprint_hidden_content', $args );
}


/**
 * Prints the custom CSS input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_custom_css_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $value = get_post_meta( (int) $post_id, 'geoprint_custom_css', true );
    $args  = array(
        'type'        => 'textarea',
        'name'        => 'geoprint_custom_css',
        'label'       => __( 'Custom CSS', 'geoprint' ),
        'class'       => 'custom-css-field',
        'default'     => '',
        'description' => __( 'Enter your custom css code here.', 'geoprint' ),
        'value'       => $value,
    );
    geoprint_render_setting_field( 'geoprint_custom_css', $args );
}


/**
 * Prints the generate cover checkbox input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_generate_cover_content_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $value = get_post_meta( (int) $post_id, 'geoprint_generate_cover', true );
    $args  = array(
        'type'        => 'checkbox',
        'name'        => 'geoprint_generate_cover',
        'label'       => __( 'Generate a cover ?', 'geoprint' ),
        'class'       => '',
        'default'     => false,
        'description' => '',
        'value'       => $value,
    );
    geoprint_render_setting_field( 'geoprint_generate_cover', $args );
}


/**
 * Prints the generate colophon checkbox input
 * 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_generate_colophon_input( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $value = get_post_meta( (int) $post_id, 'geoprint_generate_colophon', true );
    $args  = array(
        'type'        => 'checkbox',
        'name'        => 'geoprint_generate_colophon',
        'label'       => __( 'Generate a colophon ?', 'geoprint' ),
        'class'       => '',
        'default'     => false,
        'description' => '',
        'value'       => $value,
    );
    geoprint_render_setting_field( 'geoprint_generate_colophon', $args );
}


/**
 * Renders a form field
 * 
 * @param  string  $id    ID of the field. 
 * @param  array   $args  Array of arguments for the field 
 */
function geoprint_render_setting_field( $id, $args = array() ){
    $args = wp_parse_args( $args, array(
        'type'        => 'text',
        'name'        => '',
        'label'       => __( 'New field', 'geoprint' ),
        'class'       => '',
        'default'     => '',
        'description' => '',
        'var'         => '',
        'unit'        => '',
    ) );

    $value = ! empty( $args['value'] ) ? $args['value'] : $args['default'];  
    switch ( $args['type'] ) {
        case 'multi-input':
            ?>
                <fieldset class="settings-field multi-field">
                    <legend><?php echo esc_html( $args['label'] ); ?></legend>  
                    <?php foreach ( $args['fields'] as $key => $field ) : 
                        $name = sprintf( '%s[%s]', $id, $key ); 
                        $step = ! empty( $field['step'] ) ? sprintf( 'step="%s"', (float) $field['step'] ) : '';
                        $min  = ! empty( $field['min'] ) ? sprintf( 'min="%s"', (float) $field['min'] ) : '';
                        $max  = ! empty( $field['max'] ) ? sprintf( 'max="%s"', (float) $field['max'] ) : '';
                    ?>
                        <span class="settings-field <?php echo esc_attr( $field['type'] ); ?>-field">
                            <label for="<?php echo esc_attr( $name );?>" class="block"><?php echo esc_html( $field['label'] );?></label>
                            <input id="<?php echo esc_attr( $name ); ?>" class="<?php echo esc_attr( $field['class'] ); ?>" data-unit="<?php echo esc_attr( $field['unit']); ?>" data-var="<?php echo esc_attr( $field['var']); ?>" name="<?php echo esc_attr( $name ); ?>" type="<?php echo esc_attr( $field['type'] ); ?>" <?php echo $step, $min, $max; ?> value="<?php echo esc_attr( $field['value'] ); ?>"/>
                            <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                        </span>
                    <?php endforeach; ?>
                </fieldset>
            <?php
            break;
        case 'radio':
            ?>
                <fieldset class="settings-field radio-field">
                    <legend><?php echo esc_html( $args['label'] ); ?></legend>  
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                    <?php foreach ( $args['options'] as $value => $label ) : $id = sprintf( '%s-%s', $args['name'], $value );?>
                        <span class="radio-input">
                            <input id="<?php echo esc_attr( $id ); ?>" type="radio" class="<?php echo esc_attr( $args['class'] ); ?>" name="<?php echo esc_attr( $args['name'] ); ?>" data-unit="<?php echo esc_attr( $args['unit']); ?>" data-var="<?php echo esc_attr( $args['var']); ?>" value="<?php echo esc_attr( $value ); ?>" <?php checked( $value, $args['value'] ); ?> />
                            <label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $label );?></label>
                        </span>
                    <?php endforeach; ?>
                </fieldset>
            <?php
            break;
        case 'checkbox':
            ?>
                <div class="settings-field <?php echo esc_attr( $args['type'] ); ?>-field">
                    <input id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $args['class'] ); ?>" data-unit="<?php echo esc_attr( $args['unit']); ?>" data-var="<?php echo esc_attr( $args['var']); ?>" name="<?php echo esc_attr( $args['name'] ); ?>" type="<?php echo esc_attr( $args['type'] ); ?>" <?php checked( $value, 1, true ); ?> />
                    <label for="<?php echo esc_attr( $id );?>"><?php echo esc_html( $args['label'] );?></label>
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                </div>
            <?php
            break;
        case 'select':
            ?>
                <div class="settings-field select-field">
                    <label for="<?php echo esc_attr( $id );?>" style="display:block;"><?php echo esc_html( $args['label'] );?></label>
                    <select id="<?php echo esc_attr( $id );?>" name="<?php echo esc_attr( $args['name'] );?>" class="<?php echo esc_attr( $args['class'] );?>">
                        <option value=""><?php esc_html_e( 'Select', 'geoprint' ); ?></option>
                        <?php foreach ( $args['options'] as $value => $label ) : ?>
                            <option value="<?php echo esc_attr( $value );?>" <?php selected( $value, $args['value'] )?>><?php echo esc_attr( $label );?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                </div>
            <?php
            break;
        case 'textarea':
            ?>
                <div class="settings-field select-field">
                    <label for="<?php echo esc_attr( $id );?>" style="display:block;"><?php echo esc_html( $args['label'] );?></label>
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                    <textarea id="<?php echo esc_attr( $id );?>" name="<?php echo esc_attr( $args['name'] );?>" class="<?php echo esc_attr( $args['class'] );?>"><?php echo esc_textarea( $value ); ?></textarea>
                </div>
            <?php
            break;
        case 'color':
            ?>
                <div class="settings-field <?php echo esc_attr( $args['type'] ); ?>-field">
                    <label for="<?php echo esc_attr( $id );?>" class="block"><?php echo esc_html( $args['label'] );?></label>
                    <span class="color-input">
                        <input id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $args['class'] ); ?>" data-unit="<?php echo esc_attr( $args['unit']); ?>" data-var="<?php echo esc_attr( $args['var']); ?>" name="<?php echo esc_attr( $args['name'] ); ?>" type="text" value="<?php echo esc_attr( $value ); ?>"/>
                        <input class="helper-input" data-target="<?php echo esc_attr( $id ); ?>" type="<?php echo esc_attr( $args['type'] ); ?>" value="<?php echo esc_attr( $value ); ?>"/>
                    </span>
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                </div>
            <?php
            break;
        default:
            $step = ! empty( $args['step'] ) ? sprintf( 'step="%s"', (float) $args['step'] ) : '';
            $min  = ! empty( $args['min'] ) ? sprintf( 'min="%s"', (float) $args['min'] ) : '';
            $max  = ! empty( $args['max'] ) ? sprintf( 'max="%s"', (float) $args['max'] ) : '';
            ?>
                <div class="settings-field <?php echo esc_attr( $args['type'] ); ?>-field">
                    <label for="<?php echo esc_attr( $id );?>" class="block"><?php echo esc_html( $args['label'] );?></label>
                    <input id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $args['class'] ); ?>" data-unit="<?php echo esc_attr( $args['unit']); ?>" data-var="<?php echo esc_attr( $args['var']); ?>" name="<?php echo esc_attr( $args['name'] ); ?>" type="<?php echo esc_attr( $args['type'] ); ?>" <?php echo $step, $min, $max; ?> value="<?php echo esc_attr( $value ); ?>"/>
                    <?php if( ! empty( $args['description'] ) ) printf( '<em class="description" style="display:block;">%s</em>', wp_kses_post( $args['description'] ) ); ?>
                </div>
            <?php
            break;
    }
}


/**
 * Scans the plugin directory for formats files
 * 
 * @return  array  $formats  $filename => label
 */
function geoprint_get_book_formats(){
    $files = scandir( GEOPRINT_PATH . '/formats' );
    if( is_dir( get_theme_file_path() . '/geoprint/formats' ) ) $files = array_merge( scandir( get_theme_file_path() . '/geoprint/formats' ), $files );
    $files = array_values( array_unique( array_filter( $files, function( $file ){ return $file !== '..' && $file !== '.' && ! is_dir( $file ); } ) ) );

    $formats = array(
        'A4.css' => __( 'A4', 'geoprint' ),
        'A5.css' => __( 'A5', 'geoprint' ),
        'A6.css' => __( 'A6', 'geoprint' ),
        'A7.css' => __( 'A7', 'geoprint' ),
        'A8.css' => __( 'A8', 'geoprint' ),
        'B5.css' => __( 'B5', 'geoprint' ),
        'B6.css' => __( 'B6', 'geoprint' ),
        'carre85.css'  => __( 'Square 85mm', 'geoprint' ),
        'carre120.css' => __( 'Square 120mm', 'geoprint' ),
        'carre140.css' => __( 'Square 140mm', 'geoprint' ),
        'carre210.css' => __( 'Square 210mm', 'geoprint' ),
        'poche110x180.css'     => __( 'Pocket (110x180)', 'geoprint' ),
        'poche130x195.css'     => __( 'Pocket (130x195)', 'geoprint' ),
        'pli110x250.css'       => __( 'Roadmap (110x250)', 'geoprint' ),
        'dvd120x240.css'       => __( 'DVD (120x240)', 'geoprint' ),
        'fanzine127x203.css'   => __( 'Fanzine (127x203)', 'geoprint' ),
        'poesie160x240.css'    => __( 'Poetry (160x240)', 'geoprint' ),
        'revue195x280.css'     => __( 'Magazine (195x280)', 'geoprint' ),
        'essai130x208.css'     => __( 'Essay (130x208)', 'geoprint' ),
        'carnet148x210.css'    => __( 'Booklet (148x210)', 'geoprint' ),
        'graphique240x270.css' => __( 'Graphic Book (240x270)', 'geoprint' ),

    );

    $formats = array_filter( $formats, function( $key ) use( $files ) { return in_array( $key, $files ); }, ARRAY_FILTER_USE_KEY );
    return apply_filters( 'geoprint_book_formats', $formats );
}

/**
 * Converts a hex color to rgba
 * 
 * @param   string  $color
 * @return  string  $rgba_color
 */
function geoprint_hex_to_rgba( $color ){
    $color = trim( $color, '#' );
    $split = str_split( $color, 2 );
    $r = hexdec( $split[0] );
    $g = hexdec( $split[1] );
    $b = hexdec( $split[2] );
    $a = isset( $split[3] ) ? hexdec( $split[3] ) / 255 : 0.8 ;
    return sprintf( 'rgba(%d,%d,%d,%f)',$r,$g,$b,$a );
}