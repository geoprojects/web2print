<?php
/**
 * Template tags used in the preview
 */


/**
 * Display all tags, if any
 *  
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_tags( $post_id = 0, $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $html = has_tag( '', $post_id ) ? sprintf( '<aside class="tags">%s</aside>', get_the_tag_list( '' ) ) : ''; 
    if ( $echo ) echo $html;
    return $html;
}


/**
 * Display post thumbnail, if any
 * 
 * @param   string  $size 
 * @param   int     $post_id
 * @param   bool    $echo
 * @return  string  $html
 */
function geoprint_thumbnail( $size = 'post-thumbnail', $post_id = 0, $attr = [ 'loading' => 'eager', 'srcset' => '' ], $echo = true ){
    if( ! $post_id ) $post_id = get_the_ID();
    $html = get_the_post_thumbnail( $post_id, $size, $attr );
    if ( $echo ) echo $html;
    return $html;
}