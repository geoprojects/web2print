<?php
/**
 * All functions used in PagedJS template
 */

/**
 * Returns styles to insert as <link> tags.
 * 
 * @return  array  $styles
 */
function geoprint_get_template_styles(){
    $styles = array(
        'leaflet-styles'         => [ 'href'  => 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css', 'media' => 'all' ],
        'interface-styles'       => [ 'href'  => GEOPRINT_URL . 'build/css/interface.css', 'media' => 'all' ],
        'pagedjs-preview-styles' => [ 'href'  => GEOPRINT_URL . 'build/css/geoprint.css', 'media' => 'all' ],
    );

    $settings = geoprint_get_template_settings();
    $font_settings = array_filter( $settings, function( $setting ){ return 'font-family' == $setting['control'];} );
    if( ! empty( $font_settings ) ){
        $fonts_to_load = [];
        foreach ( $font_settings as $key => $setting ) {
            $value = ! empty( $setting['value'] ) ? $setting['value'] : '';
            if( $value ){
                $src = geoprint_get_font_family( $value )['src'];
                if ( $src && ! in_array( $src, $fonts_to_load ) ){
                    $fonts_to_load[] = $src;
                    $styles[$key] = [ 'href'  => $src, 'media' => 'all' ];
                }
            }
        }
    }
    return apply_filters( 'geoprint_template_styles', $styles );
}


/**
 * Returns styles to insert as <style> tags.
 * 
 * @return  array  $inline_styles
 */
function geoprint_get_template_inline_styles( $project_id = 0 ){
    if( ! $project_id ) $project_id = get_the_id();
    
    $hidden_content = get_post_meta( $project_id, 'geoprint_hidden_content', true );
    $styles = array(
        'variables' => [],
        'classes'   => [],
        'hidden'    => ! empty( $hidden_content ) && is_array( $hidden_content ) ? $hidden_content : [],
        'custom'    => '',
    );

    // Parse settings
    $settings = geoprint_get_template_settings( $project_id );
    foreach ( $settings as $key => $data ) {
        $variables = '';
        $classes   = '';
        switch ( $data['control'] ) {
            case 'margins':
                foreach ( $data['fields'] as $subfield_key => $subfield_data ) {
                    if( ! empty( $subfield_data['var'] ) ){
                        $unit       = ! empty( $subfield_data['unit'] ) ? $subfield_data['unit'] : ''; 
                        $value      = ! empty( $subfield_data['value'] ) ? sprintf( '%s%s', $subfield_data['value'], $unit ) : '';
                        $variables .= ! empty( $value ) ? sprintf( '%s: %s;', $subfield_data['var'], $value ) : '';
                    }
                }
                break;
            case 'font-family':
                $value  = isset( $data['value'] ) ? $data['value'] : $data['default'];
                $family = ! empty( $value ) ? geoprint_get_font_family( $value ) : [];
                if( ! empty( $family ) && $data['var'] ) $variables .= sprintf( '%s: %s;', $data['var'], $family['value'] );
                break;
            case 'bg-image':
                $value = isset( $data['value'] ) ? $data['value'] : $data['default'];
                $image = ! empty( $value ) ? geoprint_get_bg_image( $value ) : [];
                if( ! empty( $image ) && $data['var'] ) $variables .= sprintf( '%s: url("%s");', $data['var'], $image['src'] );
                break;
            case 'heading-size':
                // Prepare all CSS classes
                $sizes = geoprint_get_heading_size();
                foreach ( $sizes as $id => $size ) {
                    if( ! isset( $styles['classes'][$id])){
                        $css_string = '';
                        foreach ( $size['sizes'] as $var => $value ) $css_string .= sprintf( '%s: %s;', $var, $value );
                        $styles['classes'][$id] = $css_string;
                    }
                }
                
                // Handles default variable value
                $value = isset( $data['value'] ) ? $data['value'] : $data['default'];
                $size  = ! empty( $value ) ? $sizes[$value] : [];
                if( ! empty( $size['sizes'] ) ) foreach ( $size['sizes'] as $var => $value ) $variables .= sprintf( '%s: %s;', $var, $value );
                break;
            default:
                // Handles single css variable input
                if( ! empty( $data['var'] ) ){
                    $unit       = ! empty( $data['unit'] ) ? $data['unit'] : ''; 
                    $value      = ! empty( $data['value'] ) ? sprintf( '%s%s', $data['value'], $unit ) : '';
                    $variables .= ! empty( $value ) ? sprintf( '%s: %s;', $data['var'], $value ) : '';
                }
                break;
        }
        
        if( $variables ){
            $selector = 'base' === $data['section'] ? '.pagedjs_pages' : sprintf( '.pagedjs_%s_page', $data['section'] );
            $styles['variables'][$selector][] = $variables;
            if( 'bg-color' == $data['control'] ){
                $styles['variables'][$selector][] = sprintf( '%s: %s;', $data['var'] . '-rgb', geoprint_hex_to_rgba( $value ) );
            }
        }
        if( $classes ){
            $selector = 'base' === $data['section'] ? ':root' : '.' . $data['section'];
            $styles['variables'][$selector][] = $variables;
        }
    }

    // Consolidate styles strings.
    $inline_styles = array(
        'variables' => '',
        'classes'   => '',
        'hidden'    => '',
        'custom'    => '',
    );

    if( ! empty( $styles['variables'] ) ){
        foreach ( $styles['variables'] as $selector => $values ) {
            $inline_styles['variables'] .= sprintf( '%s{%s}', $selector, join('', $values ) );
        }
    }
    if( ! empty( $styles['classes'] ) ){
        foreach ( $styles['classes'] as $selector => $values ) {
            $inline_styles['classes'] .= sprintf( '.%s{%s}', $selector, $values );
        }
    }
    if( ! empty( $styles['hidden'] ) ){
        $styles['hidden'] = array_map( function( $id ){ return sprintf( '#%s', trim( $id, '#' ) ); }, $styles['hidden'] );
        $selector = join(',', $styles['hidden']);
        $inline_styles['hidden'] .= sprintf( '%s{display: none;}', $selector );
    }
    if( $css_template_id = get_post_meta( $project_id, 'chosenTemplate', true ) ){
        if( $css = get_post_meta( (int) $css_template_id , 'templateCSS', true ) ) {
            $inline_styles['template'] = $css;
        }
    }
    if( $custom_css = get_post_meta( $project_id, 'geoprint_custom_css', true ) ){
        $inline_styles['custom'] = sanitize_textarea_field( $custom_css );
    }

    return apply_filters( 'geoprint_template_inline_styles', $inline_styles );
}


/**
 * Returns scripts to insert as <script src=""> tags.
 * 
 * @return  array  $scripts
 */
function geoprint_get_template_scripts(){
    $scripts = array(
        'leaflet-script'         => [ 'src' => 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js' ],
        'interaction-script'     => [ 'src' => 'https://unpkg.com/interactjs/dist/interact.min.js' ],
        'pagedjs-preview-script' => [ 'src' => GEOPRINT_URL . 'build/js/geoprint.js' ],
    );

    return apply_filters( 'geoprint_template_scripts', $scripts );
}

/**
 * Returns an array of settings sections
 * 
 * @return  array  $sections
 */
function geoprint_get_settings_sections( $project_id = 0 ){
    if( ! $project_id && 'projects' == get_post_type() ) $project_id = get_the_id();
    
    $sections = array(
        'base'       => __( 'Base Settings', 'geoprint' ),
        'cover'      => __( 'Cover Settings', 'geoprint' ),
        'project'    => __( 'Project Settings', 'geoprint' ),
        'post'       => __( 'Post Settings', 'geoprint' ),
        'maps'       => __( 'Maps Settings', 'geoprint' ),
        'markers'    => __( 'Markers Settings', 'geoprint' ),
        'geoformat'  => __( 'Geoformat Settings', 'geoprint' ),
        'colophon'   => __( 'Colophon Settings', 'geoprint' ),
    );

    $types = geoprint_get_content_types( $project_id );
    
    // Add in markers if there are maps.
    $index = array_search( 'maps', $types );
    if ( false !== $index && ! in_array( 'markers', $types ) ) array_splice( $types, $index, 0, 'markers' );
    
    $generate_cover = get_post_meta( (int) get_the_id(), 'geoprint_generate_cover', true );
    $base_sections = $generate_cover ? [ 'base', 'cover', 'project', 'geoformat' ] : ['base', 'project', 'geoformat'];  
    $default  = array_unique( array_merge( $base_sections, $types ) );
    $generate_colophon = get_post_meta( (int) get_the_id(), 'geoprint_generate_colophon', true );
    if( $generate_colophon ) $default[] = 'colophon';
    
    $sections = array_filter( $sections, function( $key ) use( $default ) { return in_array( $key, $default ); }, ARRAY_FILTER_USE_KEY );
    return apply_filters( 'geoprint_settings_sections', $sections, $project_id );
}


/**
 * Parses content to return only all post_types
 * 
 * @param   int    $project_id
 * @return  array  $post_types
 */
function geoprint_get_content_types( $project_id = 0 ){
    if( ! $project_id && 'projects' == get_post_type() ) $project_id = get_the_id();
    
    $transient_name = sprintf( 'geoprint_content_types_%d', (int) $project_id );
    $types          = get_transient( $transient_name );
    if( ! $types ){
        $contents = geoprint_get_project_content( $project_id );
        $types    = array_unique( wp_list_pluck( $contents, 'post_type' ) );
        set_transient( $transient_name, $types, DAY_IN_SECONDS );
    }
    return apply_filters( 'geoprint_content_type', $types , $project_id );
}

/**
 * Returns settings to display in the settings form
 * 
 * @return  array  $settings
 */
function geoprint_get_template_settings( $post_id = 0 ){
    if( ! $post_id ) $post_id = get_the_id();
    $values   = get_post_meta( $post_id, 'geoprint_settings', true );
    $sections = geoprint_get_settings_sections();

    $settings = array();
    foreach ( $sections as $section => $label ) {
        $section_settings = array(
            "${section}FontSize" => array(
                'control'  => 'font-size',
                'section' => $section,
                'type'    => 'number',
                'name'    => "${section}FontSize",
                'class'   => 'css-var-input',
                'var'     => "--geoprint-font-size",
                'label'   => __( 'Text font size', 'geoprint' ),
                'default' => $section == 'base' ? 16 : '',
                'unit'    => 'px',
                'value'   => ! empty( $values["${section}FontSize"] ) ? (int) $values["${section}FontSize"] : ( $section == 'base' ? 16 : '' ) ,
            ),
            "${section}TextColor" => array(
                'control'  => 'text-color',
                'section'  => $section,
                'type'     => 'color',
                'name'     => "${section}TextColor",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-text-color',
                'label'    => __( 'Text Color', 'geoprint' ),
                'default'  => '',
                'value'    => isset( $values["${section}TextColor"] ) ? $values["${section}TextColor"] : '',
            ),
            "${section}Columns" => array(
                'control'  => 'columns',
                'section' => $section,
                'type'    => 'number',
                'name'    => "${section}Columns",
                'class'   => 'css-var-input',
                'var'     => "--geoprint-columns",
                'label'   => __( 'Columns', 'geoprint' ),
                'default' => '',
                'value'   => ! empty( $values["${section}Columns"] ) ? (int) $values["${section}Columns"] : '',
            ),
            "${section}FontFamily" => array(
                'control'  => 'font-family',
                'section'  => $section,
                'type'     => 'select',
                'name'     => "${section}FontFamily",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-font-family',
                'label'    => __( 'Font family', 'geoprint' ),
                'default'  => $section == 'base' ? 'system' : '',
                'value'    => isset( $values["${section}FontFamily"] ) ? $values["${section}FontFamily"] : ( $section == 'base' ? 'system' : '' ),
                'options'  => array_map( function( $font ){ return $font['label']; }, geoprint_get_font_family() ),
            ),
            "${section}HeadingSize" => array(
                'control'  => 'heading-size',
                'section'  => $section,
                'type'     => 'select',
                'name'     => "${section}HeadingSize",
                'class'    => 'css-class-input',
                'label'    => __( 'Heading Sizes', 'geoprint' ),
                'default'  => $section == 'base' ? 'minor-second' : '',
                'value'    => isset( $values["${section}HeadingSize"] ) ? $values["${section}HeadingSize"] : ( $section == 'base' ? 'minor-second' : '' ),
                'options'  => array_map( function( $size ){ return $size['label']; }, geoprint_get_heading_size() ),
            ),
            "${section}TextAlign" => array(
                'control'  => 'text-align',
                'section'  => $section,
                'type'     => 'radio',
                'name'     => "${section}TextAlign",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-text-align',
                'label'    => __( 'Text align', 'geoprint' ),
                'default'  => '',
                'value'    => isset( $values["${section}TextAlign"] ) ? $values["${section}TextAlign"] : '',
                'options'  => array(
                    'start'   => __( 'Left', 'geoprint' ),
                    'center'  => __( 'Center', 'geoprint' ),
                    'end'     => __( 'Right', 'geoprint' ),
                    'justify' => __( 'Justified', 'geoprint' ),
                    'inherit' => __( 'None', 'geoprint' ),
                )
            ),
            "${section}BgColor" => array(
                'control'  => 'bg-color',
                'section'  => $section,
                'type'     => 'color',
                'name'     => "${section}BgColor",
                'class'    => 'css-var-input',
                'var'      => "--geoprint-bg-color",
                'label'    => __( 'Background Color', 'geoprint' ),
                'default'  => $section == 'base' ? '#eeeeee' : '',
                'value'    => isset( $values["${section}BgColor"] ) ? $values["${section}BgColor"] : ($section == 'base' ? '#eeeeee' : ''),
            ),
            "${section}BgImage" => array(
                'control'  => 'bg-image',
                'section'  => $section,
                'type'     => 'select',
                'name'     => "${section}BgImage",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-bg-image',
                'label'    => __( 'Background Pattern', 'geoprint' ),
                'default'  => '',
                'value'    => isset( $values["${section}BgImage"] ) ? $values["${section}BgImage"] : '',
                'options'  => array_map( function( $image ){ return $image['label']; }, geoprint_get_bg_image() ),
            ),
            "${section}BgSize" => array(
                'control'  => 'bg-size',
                'section'  => $section,
                'type'     => 'select',
                'name'     => "${section}BgSize",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-bg-size',
                'label'    => __( 'Background Size', 'geoprint' ),
                'default'  => '',
                'value'    => isset( $values["${section}BgSize"] ) ? $values["${section}BgSize"] : '',
                'options'  => array(
                    'auto'    => __( 'Use original size', 'geoprint' ),
                    'cover'   => __( 'Cover whole page', 'geoprint' ),
                    'contain' => __( 'Contain into page', 'geoprint' ),
                ),
            ),
            "${section}BgPosition" => array(
                'control'  => 'bg-position',
                'section'  => $section,
                'type'     => 'select',
                'name'     => "${section}BgPosition",
                'class'    => 'css-var-input',
                'var'      => '--geoprint-bg-position',
                'label'    => __( 'Background Position', 'geoprint' ),
                'default'  => '',
                'value'    => isset( $values["${section}BgPosition"] ) ? $values["${section}BgPosition"] : '',
                'options'  => array(
                    'center' => __( 'Center', 'geoprint' ),
                    'top'    => __( 'Top', 'geoprint' ),
                    'bottom' => __( 'Bottom', 'geoprint' ),
                    'left'   => __( 'Left', 'geoprint' ),
                    'right'  => __( 'Right', 'geoprint' ),
                    'top right'     => __( 'Top Right', 'geoprint' ),
                    'top left'      => __( 'Top Left', 'geoprint' ),
                    'bottom right'  => __( 'Bottom Right', 'geoprint' ),
                    'bottom left'   => __( 'Bottom Left', 'geoprint' ),
                ),
            ),
        );
        $settings = array_merge( $settings, $section_settings );
    }

    // Additional settings
    $settings['baseMargins'] = array(
        'control'  => 'margins',
        'section'  => 'base',
        'type'     => 'multi-input',
        'name'     => 'margins',
        'label'    => __( 'Margins', 'geoprint' ),
        'default'  => array( 'top' => 15, 'botton' => 15, 'left' => 15, 'right' => 15, ),
        'fields' => array(
            'top' => array(
                'type'    => 'number',
                'var'     => '--pagedjs-margin-top',
                'label'   => __( 'Top', 'geoprint' ),
                'class'   => 'css-var-input',
                'default' => 15,
                'unit'    => 'mm',
                'value'   => ! empty( $values['baseMargins']['top'] ) ? $values['baseMargins']['top'] : 15,
            ),
            'bottom' => array(
                'type'    => 'number',
                'var'     => '--pagedjs-margin-bottom',
                'label'   => __( 'Bottom', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 15,
                'unit'    => 'mm',
                'value'   => ! empty( $values['baseMargins']['bottom'] ) ? $values['baseMargins']['bottom'] : 15,
            ),
            'left' => array(
                'type'    => 'number',
                'var'     => '--pagedjs-margin-left',
                'label'   => __( 'Left', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 15,
                'unit'    => 'mm',
                'value'   => ! empty( $values['baseMargins']['left'] ) ? $values['baseMargins']['left'] : 15,
            ),
            'right' => array(
                'type'    => 'number',
                'var'     => '--pagedjs-margin-right',
                'label'   => __( 'Right', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 15,
                'unit'    => 'mm',
                'value'   => ! empty( $values['baseMargins']['right'] ) ? $values['baseMargins']['right'] : 15,
            ),
        ),
    );
    $settings['coverTitleMargins'] = array(
        'control'  => 'margins',
        'section'  => 'cover',
        'type'     => 'multi-input',
        'name'     => 'margins',
        'label'    => __( 'Title margins', 'geoprint' ),
        'default'  => array( 'top' => 0, 'botton' => 0, 'left' => 0, 'right' => 0, ),
        'fields' => array(
            'top' => array(
                'type'    => 'number',
                'var'     => '--cover-title-margin-top',
                'label'   => __( 'Top', 'geoprint' ),
                'class'   => 'css-var-input',
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverTitleMargins']['top'] ) ? $values['coverTitleMargins']['top'] : 0,
            ),
            'bottom' => array(
                'type'    => 'number',
                'var'     => '--cover-title-margin-bottom',
                'label'   => __( 'Bottom', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverTitleMargins']['bottom'] ) ? $values['coverTitleMargins']['bottom'] : 0,
            ),
            'left' => array(
                'type'    => 'number',
                'var'     => '--cover-title-margin-left',
                'label'   => __( 'Left', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverTitleMargins']['left'] ) ? $values['coverTitleMargins']['left'] : 0,
            ),
            'right' => array(
                'type'    => 'number',
                'var'     => '--cover-title-margin-right',
                'label'   => __( 'Right', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverTitleMargins']['right'] ) ? $values['coverTitleMargins']['right'] : 0,
            ),
        ),
    );
    $settings['coverSubtitleMargins'] = array(
        'control'  => 'margins',
        'section'  => 'cover',
        'type'     => 'multi-input',
        'name'     => 'margins',
        'label'    => __( 'Subtitle margins', 'geoprint' ),
        'default'  => array( 'top' => 0, 'botton' => 0, 'left' => 0, 'right' => 0, ),
        'fields' => array(
            'top' => array(
                'type'    => 'number',
                'var'     => '--cover-subtitle-margin-top',
                'label'   => __( 'Top', 'geoprint' ),
                'class'   => 'css-var-input',
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverSubtitleMargins']['top'] ) ? $values['coverSubtitleMargins']['top'] : 0,
            ),
            'bottom' => array(
                'type'    => 'number',
                'var'     => '--cover-subtitle-margin-bottom',
                'label'   => __( 'Bottom', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverSubtitleMargins']['bottom'] ) ? $values['coverSubtitleMargins']['bottom'] : 0,
            ),
            'left' => array(
                'type'    => 'number',
                'var'     => '--cover-subtitle-margin-left',
                'label'   => __( 'Left', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverSubtitleMargins']['left'] ) ? $values['coverSubtitleMargins']['left'] : 0,
            ),
            'right' => array(
                'type'    => 'number',
                'var'     => '--cover-subtitle-margin-right',
                'label'   => __( 'Right', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['coverSubtitleMargins']['right'] ) ? $values['coverSubtitleMargins']['right'] : 0,
            ),
        ),
    );
    $settings['projectTitleMargins'] = array(
        'control'  => 'margins',
        'section'  => 'project',
        'type'     => 'multi-input',
        'name'     => 'margins',
        'label'    => __( 'Title margins', 'geoprint' ),
        'default'  => array( 'top' => 0, 'botton' => 0, 'left' => 0, 'right' => 0, ),
        'fields' => array(
            'top' => array(
                'type'    => 'number',
                'var'     => '--project-title-margin-top',
                'label'   => __( 'Top', 'geoprint' ),
                'class'   => 'css-var-input',
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectTitleMargins']['top'] ) ? $values['projectTitleMargins']['top'] : 0,
            ),
            'bottom' => array(
                'type'    => 'number',
                'var'     => '--project-title-margin-bottom',
                'label'   => __( 'Bottom', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectTitleMargins']['bottom'] ) ? $values['projectTitleMargins']['bottom'] : 0,
            ),
            'left' => array(
                'type'    => 'number',
                'var'     => '--project-title-margin-left',
                'label'   => __( 'Left', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectTitleMargins']['left'] ) ? $values['projectTitleMargins']['left'] : 0,
            ),
            'right' => array(
                'type'    => 'number',
                'var'     => '--project-title-margin-right',
                'label'   => __( 'Right', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectTitleMargins']['right'] ) ? $values['projectTitleMargins']['right'] : 0,
            ),
        ),
    );
    $settings['projectContentMargins'] = array(
        'control'  => 'margins',
        'section'  => 'project',
        'type'     => 'multi-input',
        'name'     => 'margins',
        'label'    => __( 'Content margins', 'geoprint' ),
        'default'  => array( 'top' => 0, 'botton' => 0, 'left' => 0, 'right' => 0, ),
        'fields' => array(
            'top' => array(
                'type'    => 'number',
                'var'     => '--project-content-margin-top',
                'label'   => __( 'Top', 'geoprint' ),
                'class'   => 'css-var-input',
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectContentMargins']['top'] ) ? $values['projectContentMargins']['top'] : 0,
            ),
            'bottom' => array(
                'type'    => 'number',
                'var'     => '--project-content-margin-bottom',
                'label'   => __( 'Bottom', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectContentMargins']['bottom'] ) ? $values['projectContentMargins']['bottom'] : 0,
            ),
            'left' => array(
                'type'    => 'number',
                'var'     => '--project-content-margin-left',
                'label'   => __( 'Left', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectContentMargins']['left'] ) ? $values['projectContentMargins']['left'] : 0,
            ),
            'right' => array(
                'type'    => 'number',
                'var'     => '--project-content-margin-right',
                'label'   => __( 'Right', 'geoprint' ),
                'class'   => 'css-var-input',  
                'default' => 0,
                'unit'    => 'px',
                'value'   => ! empty( $values['projectContentMargins']['right'] ) ? $values['projectContentMargins']['right'] : 0,
            ),
        ),
    );
    $settings['geoformatBorderColor'] = array(
        'control'  => 'border-color',
        'section'  => 'geoformat',
        'type'     => 'color',
        'name'     => "geoformatBorderColor",
        'class'    => 'css-var-input',
        'var'      => "--geoprint-border-color",
        'label'    => __( 'Border Color', 'geoprint' ),
        'default'  => '',
        'value'    => isset( $values["geoformatBorderColor"] ) ? $values["geoformatBorderColor"] : '',
    );
    $settings['coverTitleOrientation'] = array(
        'control'  => 'text-orientation',
        'section'  => 'cover',
        'type'     => 'radio',
        'name'     => 'coverTitleOrientation',
        'class'    => 'css-var-input',
        'var'      => '--geoprint-title-orientation',
        'label'    => __( 'Title orientation', 'geoprint' ),
        'default'  => '',
        'value'    => isset( $values['coverTitleOrientation'] ) ? $values['coverTitleOrientation'] : '',
        'options'  => array(
            'horizontal-tb' => __( 'Horizontal', 'geoprint' ),
            'vertical-lr'   => __( 'Vertical', 'geoprint' ),
        ),
    );
    $settings['mapsOrientation'] = array(
        'control'  => 'orientation',
        'section'  => 'maps',
        'type'     => 'radio',
        'name'     => "mapsOrientation",
        'class'    => 'css-var-input',
        'var'      => "--geoprint-maps-orientation",
        'label'    => __( 'Maps orientation', 'geoprint' ),
        'default'  => '0deg',
        'value'    => isset( $values["mapsOrientation"] ) ? $values["mapsOrientation"] : '',
        'options'  => array(
            '0deg' => __( 'Horizontal', 'geoprint' ),
            '-90deg'   => __( 'Vertical', 'geoprint' ),
        ),
    );

    return apply_filters( 'geoprint_template_settings', $settings );
}


/**
 * Saves template settings when submitting preview settings form
 * 
 * @param  int  $post_id
 */
function geoprint_save_template_settings( $post_id = 0 ){
    if( ! $post_id ) $post_id = get_the_ID();
    if( ! isset( $_POST['geoprint_save_template_settings_nonce'] ) || ! wp_verify_nonce( $_POST['geoprint_save_template_settings_nonce'], 'geoprint_save_template_settings' ) ) return;
    if( 'projects' !== get_post_type( $post_id ) ) return;
    if( ! current_user_can( 'edit_post', (int) $post_id  ) ) return;
    
    // Save content order
    $contents = geoprint_get_project_content( (int) $post_id, [ 'fields' => 'ids' ] );
    if( ! empty( $contents ) && is_array( $contents ) ){
        foreach ( $contents as $index => $id ) {
            if( isset( $_POST[ "paged-order-${id}" ] ) ) update_post_meta( (int) $id, 'paged-order', (int) $_POST[ "paged-order-${id}" ] );
        }
    }

    // Save CSS template setting
    $chosen_template = isset( $_POST['chosenTemplate'] ) ? (int) $_POST['chosenTemplate'] : 0;
    update_post_meta( $post_id, 'chosenTemplate', $chosen_template);

    // Save book format settings
    $format = isset( $_POST['geoprint_book_format'] ) ? sanitize_file_name( $_POST['geoprint_book_format'] ) : '';
    update_post_meta( $post_id, 'geoprint_book_format', $format);

    // Save hidden content
    $value = ! empty( $_POST['geoprint_hidden_content'] ) ? array_map( 'trim', explode( ',', $_POST['geoprint_hidden_content'] ) ) : '';
    update_post_meta( $post_id, 'geoprint_hidden_content', $value );
    
    // Save custom CSS
    $value = ! empty( $_POST['geoprint_custom_css'] ) ? sanitize_textarea_field( $_POST['geoprint_custom_css'] ) : '';
    update_post_meta( $post_id, 'geoprint_custom_css', $value );

    // Save map data
    $map_data = ! empty( $_POST['geoprint_map_data'] ) ? json_decode( stripslashes( $_POST['geoprint_map_data'] ), true ) : [];
    if( ! empty( $map_data ) && is_array( $map_data ) ){
        foreach ( $map_data as $id => $value ) {
            update_post_meta( (int) $id, 'geoprint_map_settings', $value );
        }
    }

    // Save generate cover setting
    $generate_cover = isset( $_POST['geoprint_generate_cover'] );
    update_post_meta( $post_id, 'geoprint_generate_cover', $generate_cover );
    
    // Save generate colophon setting
    $generate_colophon = isset( $_POST['geoprint_generate_colophon'] );
    update_post_meta( $post_id, 'geoprint_generate_colophon', $generate_colophon );

    // Save print settings
    $settings = geoprint_get_template_settings( $post_id );
    foreach ( $settings as $id => $data ) {
        if( ! empty ( $_POST[$id] ) ){
            $values[$id] = is_array( $_POST[$id] ) ? array_map( 'sanitize_text_field', $_POST[$id] ) : sanitize_text_field( $_POST[$id] );
        } else {
            $values[$id] = $data['default'];
        }
        if( 'columns' == $data['control'] ){
            if( 0 === (int) $values[$id] || 1 === (int) $values[$id] ) $values[$id] = '';
        }
    }
    update_post_meta( $post_id, 'geoprint_settings', $values );
}


/**
 * Returns an array of heading sizes
 * 
 *  @param   string  $size 
 *  @return  array   $size|$sizes  All sizes or single size data  
 */
function geoprint_get_heading_size( $size = null ){
    $sizes = array(
        'minor-second' => array(
            'label' => __( 'Minor second', 'geoprint' ),
            'sizes' => array(
                '--geoprint-h1-size' => '1.383rem',
                '--geoprint-h2-size' => '1.296rem',
                '--geoprint-h3-size' => '1.215rem',
                '--geoprint-h4-size' => '1.138rem',
                '--geoprint-h5-size' => '1.067rem',
                '--geoprint-h6-size' => '1rem',
            ),
        ),
        'major-second' => array(
            'label' => __( 'Major second', 'geoprint' ),
            'sizes' => array(
                '--geoprint-h1-size' => '1.802rem',
                '--geoprint-h2-size' => '1.602rem',
                '--geoprint-h3-size' => '1.424rem',
                '--geoprint-h4-size' => '1.266rem',
                '--geoprint-h5-size' => '1.125rem',
                '--geoprint-h6-size' => '1rem',
            ),
        ),
        'minor-third' => array(
            'label' => __( 'Minor third', 'geoprint' ),
            'sizes' => array(
                '--geoprint-h1-size' => '2.488rem',
                '--geoprint-h2-size' => '2.074rem',
                '--geoprint-h3-size' => '1.728rem',
                '--geoprint-h4-size' => '1.44rem',
                '--geoprint-h5-size' => '1.2rem',
                '--geoprint-h6-size' => '1rem',
            ),
        ),
        'major-third' => array(
            'label' => __( 'Major third', 'geoprint' ),
            'sizes' => array(
                '--geoprint-h1-size' => '3.052rem',
                '--geoprint-h2-size' => '2.441rem',
                '--geoprint-h3-size' => '1.953rem',
                '--geoprint-h4-size' => '1.563rem',
                '--geoprint-h5-size' => '1.25rem',
                '--geoprint-h6-size' => '1rem',
            ),
        ),
        'perfect-fourth' => array(
            'label' => __( 'Perfect fourth', 'geoprint' ),
            'sizes' => array(
                '--geoprint-h1-size' => '4.209rem',
                '--geoprint-h2-size' => '3.157rem',
                '--geoprint-h3-size' => '2.369rem',
                '--geoprint-h4-size' => '1.777rem',
                '--geoprint-h5-size' => '1.33rem',
                '--geoprint-h6-size' => '1rem',
            ),
        ),
    );
    $value = array_key_exists( $size, $sizes ) ? $sizes[$size] : $sizes;
    return apply_filters( 'geoprint_heading_size', $value, $size );
}


/**
 * Returns array of available font families.
 * 
 *  @param   string  $font         Specific font data to get 
 *  @return  array   $font|$fonts  All fonts or single font data    
 */
function geoprint_get_font_family( $font = null ){
    $fonts = array(
        'system' => array(
            'label'   => __( 'System font', 'geoprint' ),
            'src'     => '',
            'value'   => "system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'montserrat' => array(
            'label'   => __( 'Montserrat', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Montserrat&display=swap',
            'value'   => "Montserrat, system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'open-sans' => array(
            'label'   => __( 'Open sans', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Open+Sans&display=swap',
            'value'   => "'Open Sans', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'garamond' => array(
            'label'   => __( 'Garamond', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=EB+Garamond:ital@0;1&display=swap',
            'value'   => "'EB Garamond', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'source-code-pro' => array(
            'label'   => __( 'Source Code Pro', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Source+Code+Pro:ital@0;1&display=swap',
            'value'   => "'Source Code Pro', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'press-start' => array(
            'label'   => __( 'Press start 2P', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap',
            'value'   => "'Press Start 2P', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'quattrocento' => array(
            'label'   => __( 'Quattrocento sans', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Quattrocento+Sans:ital@0;1&display=swap',
            'value'   => "'Quattrocento Sans', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'bodoni-moda' => array(
            'label'   => __( 'Bodoni Moda', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Bodoni+Moda:ital,opsz@0,6..96;1,6..96',
            'value'   => "'Bodoni Moda', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'libre-bodoni' => array(
            'label'   => __( 'Libre Bodoni', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Libre+Bodoni:ital@0;1&display=swap',
            'value'   => "'Libre Bodoni', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'josefin-slab' => array(
            'label'   => __( 'Josefin Slab', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Josefin+Slab:ital@0;1&display=swap',
            'value'   => "'Josefin Slab', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'ubuntu-mono' => array(
            'label'   => __( 'Ubuntu Mono', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Ubuntu+Mono:ital@0;1&display=swap',
            'value'   => "'Ubuntu Mono', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'special-elite' => array(
            'label'   => __( 'Special Elite', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Special+Elite&display=swap',
            'value'   => "'Special Elite', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'monoton' => array(
            'label'   => __( 'Monoton', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Monoton&display=swap',
            'value'   => "'Monoton', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'baskervville' => array(
            'label'   => __( 'Baskervville', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Baskervville:ital@0;1&display=swap',
            'value'   => "'Baskervville', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'black-ops-one' => array(
            'label'   => __( 'Black Ops One', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Black+Ops+One&display=swap',
            'value'   => "'Black Ops One', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'anonymous-pro' => array(
            'label'   => __( 'Anonymous Pro', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Anonymous+Pro:ital@0;1&display=swap',
            'value'   => "'Anonymous Pro', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'fredericka-the-great' => array(
            'label'   => __( 'Fredericka the Great', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap',
            'value'   => "'Fredericka the Great', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'recursive' => array(
            'label'   => __( 'Recursive', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Recursive&display=swap',
            'value'   => "'Recursive', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'ibarra-real-nova' => array(
            'label'   => __( 'Ibarra Real Nova', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Ibarra+Real+Nova:ital@0;1&display=swap',
            'value'   => "'Ibarra Real Nova', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
        'codystar' => array(
            'label'   => __( 'Codystar', 'geoprint' ),
            'src'     => 'https://fonts.googleapis.com/css2?family=Codystar&display=swap',
            'value'   => "'Codystar', system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
        ),
    );
    $value = array_key_exists( $font, $fonts ) ? $fonts[$font] : $fonts;
    return apply_filters( 'geoprint_font_families', $value, $font );
}


/**
 * Returns array of available background images.
 * 
 *  @param   string  $image          Specific image data to get 
 *  @return  array   $image|$images  All images or single image data    
 */
function geoprint_get_bg_image( $image = '' ){
    $images = array(
        'bank-note' => array(
            'label' => __( 'Bank note', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/bank-note.svg',
        ),
        'bamboo' => array(
            'label' => __( 'Bamboo', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/bamboo.svg',
        ),
        'diagonal-lines' => array(
            'label' => __( 'Diagonal lines', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/diagonal-lines.svg',
        ),
        'graph-paper' => array(
            'label' => __( 'Graph paper', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/graph-paper.svg',
        ),
        'lisbon' => array(
            'label' => __( 'Lisbon', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/lisbon.svg',
        ),
        'morphing-diamonds' => array(
            'label' => __( 'Morphing Diamonds', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/morphing-diamonds.svg',
        ),
        'pixel-dots' => array(
            'label' => __( 'Pixel Dots', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/pixel-dots.svg',
        ),
        'plus' => array(
            'label' => __( 'Plus', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/plus.svg',
        ),
        'rain' => array(
            'label' => __( 'Rain', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/rain.svg',
        ),
        'temple' => array(
            'label' => __( 'Temple', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/temple.svg',
        ),
        'texture' => array(
            'label' => __( 'Texture', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/texture.svg',
        ),
        'topography' => array(
            'label' => __( 'Topography', 'geoprint' ),
            'src'   => trailingslashit( GEOPRINT_URL ) . 'icons/topography.svg',
        ),
    );
    $value = array_key_exists( $image, $images ) ? $images[$image] : $images;
    return apply_filters( 'geoprint_bg_images', $value, $image );
}
